import { Component, OnInit } from '@angular/core';

declare let L;
declare let tomtom: any;


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  location: any;
  locationCircle: any;


//note: untuk center variabel nanti diganti lat and long untuk exact map posisi sekarang
  ngOnInit(){
    const map = tomtom.map('myMap', {
      key: 'wG9aAWX6F2wZFXzXAi9KuR3wnbhaMsp1',
      basepath: '../../assets/tomtom',
      center: [ -6.1717945, 106.909909],
      zoom: 15,

    });
  }
  getCurrentLocationAndPanToLocation(map) {
        this.location.getCurrentPosition().then((resp) => {
            map.setZoom(17);
            map.panTo(new tomtom.map.LatLng(resp.coords.latitude, resp.coords.longitude));
            this.addOverlayMyLocation(this.map, new tomtom.maps.LatLng(resp.coords.latitude, resp.coords.longitude), resp.coords.accuracy);
        });
    }
  map(map: any, arg1: any, accuracy: any) {
    throw new Error("Method not implemented.");
  }
  myLocation() {
    this.getCurrentLocationAndPanToLocation(this.map);
}

  addOverlayMyLocation(map, position, radius) {
  if (this.locationCircle != undefined) {
      this.locationCircle.setMap(null);
  }
  this.locationCircle = new tomtom.maps.Circle({
      center: position,
      clickable: false,
      draggable: false,
      editable: false,
      fillColor: '#004de8',
      fillOpacity: 0.27,
      map: map,
      radius: radius,
      strokeColor: '#004de8',
      strokeOpacity: 0.62,
      strokeWeight: 1
  });
}


}
